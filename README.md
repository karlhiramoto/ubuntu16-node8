# ubuntu16-node8
Docker container for ubuntu 16.04 LTS  with node 8.X


Published on:
  * Docker hub https://hub.docker.com/r/karlhiramoto/ubuntu16-node8/
  * Bitbucket:  https://bitbucket.org/karlhiramoto/ubuntu16-node8/
